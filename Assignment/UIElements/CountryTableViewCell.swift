//
//  CountryTableViewCell.swift
//  Assignment
//
//  Created by Thiwanka Nawarathne on 4/23/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {

    @IBOutlet weak var CountryNameLabel: UILabel!
    @IBOutlet weak var CountryShortCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
