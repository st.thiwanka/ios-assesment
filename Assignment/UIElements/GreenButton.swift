//
//  GreenButton.swift
//  Assignment
//
//  Created by Thiwanka Nawarathne on 4/24/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import UIKit

@IBDesignable class GreenButton: UIButton {
    
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10);
    let cornerRadius : CGFloat = 4.0
    let BtnBackgroundColor = UIColor(red:0.00, green:0.78, blue:0.33, alpha:1.0).cgColor
    
    override func draw(_ rect: CGRect) {
        self.setTitleColor(UIColor.white, for: .normal)
        let btnText = self.titleLabel?.text
        self.setTitle(btnText?.uppercased(), for: .normal)
        self.titleLabel?.text = self.titleLabel?.text?.uppercased()
        
        self.layer.backgroundColor = BtnBackgroundColor
        self.layer.cornerRadius = cornerRadius
        self.layer.masksToBounds = true
    }

}
