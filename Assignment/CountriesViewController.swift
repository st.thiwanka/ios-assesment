//
//  CountriesViewController.swift
//  Assignment
//
//  Created by Thiwanka Nawarathne on 4/22/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CountriesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let API_URL = "http://services.groupkt.com/country/get/all"
    var Countries = [Country]()

    @IBOutlet weak var CountryList: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if NetworkReachabilityManager()!.isReachable {
            Alamofire.request(API_URL).responseJSON { (responseData) -> Void in
                if let jsonValue = responseData.result.value {
                    let json = JSON(jsonValue)
                    let results = json["RestResponse"]["result"]
                    
                    let CountriesArray : NSArray  = results.array! as NSArray
                    for i in 0..<CountriesArray.count{
                        let country = JSON(CountriesArray[i])
                        
                        //adding country values to the country list
                        self.Countries.append(Country(
                            name: country["name"].string,
                            alpha2_code: country["alpha2_code"].string,
                            alpha3_code: country["alpha3_code"].string
                        ))
                        
                    }
                    
                    self.CountryList.isHidden = false
                    self.CountryList.reloadData()
                }
            }
        }else{
            let Alert = UIAlertController(title: "Network Error Occurred!", message: "Please Check Your Network Connection", preferredStyle: .alert)
            Alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
            self.present(Alert, animated: true)
        }
        
        self.CountryList.isHidden = false
        self.CountryList.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CountryList.isHidden = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Country") as! CountryTableViewCell
        
        let country: Country
        country = Countries[indexPath.row]
        
        cell.CountryNameLabel.text = country.name
        cell.CountryShortCode.text = country.alpha2_code
        return cell
    }
}
