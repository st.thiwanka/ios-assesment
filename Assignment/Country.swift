//
//  Country.swift
//  Assignment
//
//  Created by Thiwanka Nawarathne on 4/23/18.
//  Copyright © 2018 Thiwanka Nawarathne. All rights reserved.
//

import Foundation

class Country {
    
    var name: String?
    var alpha2_code: String?
    var alpha3_code: String?
    
    init(name: String?, alpha2_code: String?, alpha3_code: String?) {
        self.name = name
        self.alpha2_code = alpha2_code
        self.alpha3_code = alpha3_code
    }
}
