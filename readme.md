
## Get Location (Assessment)

This is an assignment done by A.S.T. Thiwanka Nawarathne as a result of applying for the position – Software Engineer (iOS). The project duration is 5 days.

#### Getting Started:

All the files are found in the Git Repository “https://gitlab.com/st.thiwanka/ios-assesment.git”. You may clone it to your computer or download as a zip file and extract. Then it should be run in Xcode (9.3).

#### Prerequisites:

-	Your device should be connected to the Internet.
-	Swift 4.1, Xcode 9.3, iOS 10 +

#### Built With:

-	'SwiftyJSON'
-	'GoogleMaps', '= 2.1.1'
-	'GooglePlaces', '= 2.7.30514.0'
-	'Alamofire'


#### Settings:

If the Google exceeds the limit of requests, change the provided API Keys (var googleAPIKey) in the file - AppDelegate.swift.

**Keys Provided:**

-	"AIzaSyCKyjNbtB5iVXnykZ6yRCFSy1lK9kmSEnw”
-	"AIzaSyDehU3JXM9SiFUsPUVKsBj-1hjlZxjnH10"

In ViewController.swift, change the “CurrentLocationMarker” to activate or deactivate the current location marker.

To activate/ deactivate the theme, set the value of “GoogleTheme”.

The style file of the theme is found in UIElements -> style.json.

#### Version:

This is the first version (V1.0).

#### Author:
A.S.T. Thiwanka Nawarathne (st.thiwanka@live.com)